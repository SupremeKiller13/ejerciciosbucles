package org.cuatrovientos.ed.bucles;

import java.util.Scanner;

public class Ejercicio05Bucles {

	public static void main(String[] args) {
		// Este es el Ejercicio 5 de Bucles
		
		Scanner readFromConsole = new Scanner(System.in);
		
		int lineas = 0;
		
		System.out.println("Escribe el n�mero de l�neas a dibujar en consola: ");
		lineas = readFromConsole.nextInt();
 
		for(int i=0;i<lineas;i++){
			
			for(int j=0;j<lineas;j++){
				
				System.out.print("*");
			}
			System.out.print("\n"); // Salto de l�nea
		}

	}

}
