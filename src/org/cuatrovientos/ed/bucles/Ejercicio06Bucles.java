package org.cuatrovientos.ed.bucles;

import java.util.Scanner;

public class Ejercicio06Bucles {

	public static void main(String[] args) {
		// Este es el Ejercicio 6 de Bucles
		
		Scanner readFromConsole = new Scanner(System.in);
		
		int numero;
		int factorial = 1;
		int resultado;
		
		System.out.println("Introduce el n�mero:");
		numero=readFromConsole.nextInt();
		
		resultado=numero;
		
		while(factorial < numero){
		resultado=resultado*(numero-factorial);
		factorial = factorial + 1;
		}
		System.out.println("El factorial de "+numero+" es: "+resultado);
	}

}
