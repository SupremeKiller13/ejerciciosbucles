package org.cuatrovientos.ed.bucles;

import java.util.Scanner;

public class Ejercicio04Bucles {

	public static void main(String[] args) {
		// Este es el Ejercicio 4 de Bucles
		
		Scanner readFromConsole = new Scanner(System.in);
		String palabra;
		
		do {
			System.out.println("Escribe una palabra 'out!': ");
		palabra = readFromConsole.nextLine();
		} while (!(palabra.equals("out!")));

	}

}
