package org.cuatrovientos.ed.bucles;

import java.util.Scanner;

public class Ejercicio02Bucles {

	public static void main(String[] args) {
		// Este es el Ejercicio 2 de Bucles
		
		int numero;
		
		Scanner readFromConsole = new Scanner(System.in);
		System.out.println("Introduce un n�mero: ");
		numero = readFromConsole.nextInt();
		
	if (numero > 0 && numero%2==0){
		for (int i = 0; i < numero; i++) {
			System.out.print("*");
		}
	}else {
        	System.exit(0);
        }

	}

}
